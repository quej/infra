---
###########################################
# Needed for Ubuntu images on Scaleway... #
# But can only be used for the FIRST run. #
#                                         #
# - hosts: all                            #
#   remote_user: root                     #
#   vars:                                 #
#     ansible_user: root                  #
#   tasks:                                #
#     - name: Make sure sudo is installed #
#       apt:                              #
#         name: sudo                      #
#       ignore_errors: yes                #
###########################################

- hosts: all
  remote_user: "{{ remote_user|default(ansible_user) }}"
  become: yes
  roles:
    # Common roles to install on all servers
    - role: common
      tags: [ common ]
    - role: netdata
      tags: [ netdata ]

    # Conditionnal roles depending on the group of the server
    - role: git
      when: "'git' in group_names"
      tags: [ git ]
    - role: certbot
      when: "'webserver' in group_names"
      tags: [ certbot, webserver ]
    - role: nginx
      when: "'webserver' in group_names"
      tags: [ nginx, webserver ]
    - role: nextcloud
      when: "'nextcloud' in group_names"
      tags: [ nextcloud, php ]
    - role: bookstack
      when: "'bookstack' in group_names"
      tags: [ bookstack, php ]
    - role: simplesamlphp
      when: "'simplesamlphp' in group_names"
      tags: [ simplesamlphp, php ]
    - role: postgresql
      when: "'postgresql' in group_names"
      tags: [ database, postgresql ]
    - role: mariadb
      when: "'mariadb' in group_names"
      tags: [ database, mariadb ]
    - role: discourse
      when: "'discourse' in group_names"
      tags: [ discourse ]
    - role: rocketchat
      when: "'rocketchat' in group_names"
      tags: [ rocketchat ]
    - role: bbb
      when: "'bbb' in group_names"
      tags: [ bbb ]
  post_tasks:
    # TODO: move this upstream in the Nginx ansible role
    - name: Create basic auth password file for monitoring dashboards
      copy:
        dest: "/etc/nginx/passwords"
        content: "{{ nginx_passwords }}"
        owner: "{{ __nginx_user }}"
        mode: '0400'
      no_log: true
      when:
        - "'webserver' in group_names"
        - nginx_passwords is defined
      tags: [ webserver, nginx ]

    # TODO: move this upstream in the SimpleSAMLphp ansible role
    - name: Install extra SSP authentication module for Discourse
      git:
        repo: 'https://github.com/swcc/simplesamlphp-module-authdiscourse.git'
        dest: "{{ simplesamlphp_destination }}/simplesamlphp/modules/authdiscourse"
        force: yes
        version: "66e5d76"
      when:
        - "'simplesamlphp' in group_names"
        - simplesamlphp_identity_provider is defined
        - simplesamlphp_identity_provider['auth'] is defined
        - simplesamlphp_identity_provider['auth']['type'] == 'authdiscourse:Discourse'
      tags: [ simplesamlphp, php ]
    - name: Install extra theme for simplesamlphp
      git:
        repo: 'https://gitlab.com/interfoodcoop/simplesamlphp-theme.git'
        dest: "{{ simplesamlphp_destination }}/simplesamlphp/modules/interfoodcoop"
        force: yes
        version: "5cf02adbc6114a5fc4584a4c4b7b317e59d5aac0"
      when:
        - "'simplesamlphp' in group_names"
        -  simplesamlphp_config['theme.use'] == 'interfoodcoop:simple'
      tags: [ simplesamlphp, php ]

    # TODO: move this upstream in the matrix-docker-ansible-deploy playbook
    - name: Install extra saml2 attribute mapping file needed for Matrix Saml
      copy:
        dest: "/matrix/synapse/config/saml2-attribute-maps/map.py"
        content: |
          MAP = {
              "identifier": "urn:oasis:names:tc:SAML:2.0:attrname-format:basic",
              "fro": {
                  'discourse.username': 'uid',
                  'discourse.name': 'displayName',
                  'discourse.email': 'email',
                  'discourse.groups': 'groups'
              },
              "to": {
                  'uid': 'discourse.username',
                  'displayName': 'discourse.name',
                  'email': 'discourse.email',
                  'groups': 'discourse.groups'
              },
          }
      when:
        - "'matrix' in group_names"
        - matrix_sso is defined and matrix_sso
      tags: [ matrix, setup-all ]

    # TODO: move this upstream in the ansible-netdata role
    - name: Configure the nginx netdata plugin
      copy:
        dest: "/etc/netdata/python.d/nginx.conf"
        content: |
          localhost:
            name : 'local'
            url  : 'http://localhost/stub_status'
      when: "'webserver' in group_names"
      tags: [ nginx, webserver ]

    # Todo: move this upstream in a Netdata-config role
    - name: Configure the web_log netdata plugin
      blockinfile:
        path: /etc/netdata/python.d/web_log.conf
        block: |
          nginx_log:
            name: 'nginx'
            path: '/var/log/nginx/access.log'
          nginx_log_ssp:
            name: 'simplesamlphp'
            path: '/var/log/nginx/simplesamlphp.log'
          nginx_log_nextcloud:
            name: 'nextcloud'
            path: '/var/log/nginx/nextcloud.log'
          nginx_log_bookstack:
            name: 'bookstack'
            path: '/var/log/nginx/bookstack.log'
      when: "'webserver' in group_names"
      tags: [ nginx, webserver ]

# Playbook to install Matrix (on 'matrix_servers' group)
- import_playbook: matrix-docker-ansible-deploy/setup.yml

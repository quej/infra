---
#############################################################################
# Basically all tasks defined in                                            #
# https://www.bookstackapp.com/docs/admin/installation/#manual-installation #
#############################################################################
- name: Install needed tools
  apt:
    name:
      - git
      - composer
      - "php{{ php_fpm_version }}-tidy"

- name: Fetch release version of Bookstack
  become: true
  git:
    repo: https://github.com/BookStackApp/BookStack.git
    dest: "{{ bookstack_home_dir }}"
    version: "{{ bookstack_git_version | default('release') }}"
  notify: Run db migrations

- name: Install php dependencies
  composer:
    command: install
    working_dir: "{{ bookstack_home_dir }}"

- name: Configure bookstack
  template:
    src: .env.j2
    dest: "{{ bookstack_home_dir }}/.env"
  no_log: true

- name: Check whether a secret App key has been generated
  command: grep -Fxq "APP_KEY={{ bookstack_default_app_key }}" "{{ bookstack_home_dir }}/.env"
  register: checkdefaultappkey
  check_mode: no
  ignore_errors: yes
  changed_when: no

- name: Generate a random secret application key
  command: php artisan key:generate --force
  args:
    chdir: "{{ bookstack_home_dir }}"
  when: checkdefaultappkey.rc == 0

- name: Set writable directories for web user
  file:
    path: "{{ bookstack_home_dir }}/{{ item }}"
    owner: "{{ bookstack_user }}"
    group: "{{ bookstack_group }}"
    recurse: true
  with_items:
    - public
    - storage
    - bootstrap/cache

- name: Ensure a locale exists
  locale_gen:
    name: "{{ item }}"
    state: present
  with_items: "{{ bookstack_system_locales }}"

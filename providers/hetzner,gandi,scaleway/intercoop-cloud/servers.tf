locals {
  # Hetzner has 3 Datacenters
  # https://www.hetzner.com/unternehmen/rechenzentrum/
  location = "nbg1" # nbg1 fsn1 hel1
  # Hetzner Cloud server types
  # https://www.hetzner.com/cloud
  extra_extra_small_server_type = "cx11"  # €2.80/month TTC (1vCPU,2GB RAM,40GB disk)
  extra_small_server_type       = "cpx11" # €4.20/month TTC (2vCPU,2GB RAM,40GB disk)
  small_server_type             = "cpx21" # €8.28/month TTC (3vCPU,4GB RAM,80GB disk)
  medium_server_type            = "cpx31" # €14.88/month TTC (4vCPU,8GB RAM,160GB disk)
  big_server_type               = "cpx41" # €27.48/month TTC (8vCPU,16GB RAM,240GB disk)

  # Number of Nextcloud servers
  nextcloud_server_count = 1
  # Number of Discourse servers
  discourse_server_count = 0
  # Number of Rocket.chat servers
  rocket_server_count = 1
  # Number of Matrix servers
  matrix_server_count = 1
  # Number of Bbb servers
  bbb_server_count = 0
}

# Create a machine for Nextcloud
resource "hcloud_server" "nextcloud" {
  count       = local.nextcloud_server_count
  name        = "nextcloud${count.index + 1}.interfoodcoop.net"
  server_type = local.extra_small_server_type
  image       = "debian-10"
  location    = local.location

  ssh_keys = [hcloud_ssh_key.paul_polonium.id, hcloud_ssh_key.yvon_vieux_toshiba.id]

  backups = true

  labels = {
    os  = "debian"
    env = "intercoop-cloud"
  }

  lifecycle {
    ignore_changes = [image, labels, ssh_keys]
  }
}

resource "hcloud_server_network" "nextcloud-private-ip" {
  count     = local.nextcloud_server_count
  server_id = element(hcloud_server.nextcloud.*.id, count.index)
  subnet_id = hcloud_network_subnet.private-subnet.id
}

# Create a machine for Discourse
resource "hcloud_server" "discourse" {
  count       = local.discourse_server_count
  name        = "discourse${count.index + 1}.interfoodcoop.net"
  server_type = local.extra_small_server_type
  image       = "debian-10"
  location    = local.location

  ssh_keys = [hcloud_ssh_key.paul_polonium.id, hcloud_ssh_key.yvon_vieux_toshiba.id]

  backups = true

  labels = {
    os  = "debian"
    env = "intercoop-cloud"
  }

  lifecycle {
    ignore_changes = [image, labels, ssh_keys]
  }
}

resource "hcloud_server_network" "discourse-private-ip" {
  count     = local.discourse_server_count
  server_id = element(hcloud_server.discourse.*.id, count.index)
  subnet_id = hcloud_network_subnet.private-subnet.id
}

# Create a machine for Rocket.Chat
resource "hcloud_server" "rocket" {
  count       = local.rocket_server_count
  name        = "rocket${count.index + 1}.interfoodcoop.net"
  server_type = local.extra_small_server_type
  image       = "debian-10"
  location    = local.location

  ssh_keys = [hcloud_ssh_key.paul_polonium.id, hcloud_ssh_key.yvon_vieux_toshiba.id]

  backups = true

  labels = {
    os  = "debian"
    env = "intercoop-cloud"
  }

  lifecycle {
    ignore_changes = [image, labels, ssh_keys]
  }
}

resource "hcloud_server_network" "rocket-private-ip" {
  count     = local.rocket_server_count
  server_id = element(hcloud_server.rocket.*.id, count.index)
  subnet_id = hcloud_network_subnet.private-subnet.id
}

# Create a machine for Matrix
resource "hcloud_server" "matrix" {
  count       = local.matrix_server_count
  name        = "matrix${count.index + 1}.interfoodcoop.net"
  server_type = local.extra_small_server_type
  image       = "debian-10"
  location    = local.location

  ssh_keys = [hcloud_ssh_key.paul_polonium.id, hcloud_ssh_key.yvon_vieux_toshiba.id]

  backups = true

  labels = {
    os  = "debian"
    env = "intercoop-cloud"
  }

  lifecycle {
    ignore_changes = [image, labels, ssh_keys]
  }
}

resource "hcloud_server_network" "matrix-private-ip" {
  count     = local.matrix_server_count
  server_id = element(hcloud_server.matrix.*.id, count.index)
  subnet_id = hcloud_network_subnet.private-subnet.id
}

# This is a snapshot of a BBB server already configured to run
# It was created on the 13th of november by hand on the Hetzner web console.
# The label 'app=bbb' was added by hand too.
data "hcloud_image" "latest_bbb_snapshot" {
  with_selector = "app=bbb"
  most_recent   = true
}

# Create a machine for Bbb
resource "hcloud_server" "bbb" {
  count       = local.bbb_server_count
  name        = "bbb${count.index + 1}.interfoodcoop.net"
  server_type = local.medium_server_type

  # image = "ubuntu-16.04"
  image    = data.hcloud_image.latest_bbb_snapshot.id
  location = local.location

  ssh_keys = [hcloud_ssh_key.paul_polonium.id, hcloud_ssh_key.yvon_vieux_toshiba.id]

  backups = false

  labels = {
    os  = "ubuntu"
    env = "intercoop-cloud"
  }

  lifecycle {
    ignore_changes = [image, labels, ssh_keys]
  }
}

# Assign a “static” public ip
resource "hcloud_floating_ip" "bbb_public_ip" {
  type      = "ipv4"
  count     = 1
  server_id = length(hcloud_server.bbb.*.id) > 0 ? element(hcloud_server.bbb.*.id, count.index) : null
}

# Add server in private network
resource "hcloud_server_network" "bbb-private-ip" {
  count     = local.bbb_server_count
  server_id = element(hcloud_server.bbb.*.id, count.index)
  subnet_id = hcloud_network_subnet.private-subnet.id
}

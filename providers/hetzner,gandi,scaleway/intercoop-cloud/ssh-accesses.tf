############################################################
# List of public SSH keys which have access to the servers #
############################################################

resource "hcloud_ssh_key" "paul_polonium" {
  name       = "paul_polonium"
  public_key = file("../../../public_keys/paul.pub")
}

resource "hcloud_ssh_key" "yvon_vieux_toshiba" {
  name       = "yvon_vieux_toshiba"
  public_key = file("../../../public_keys/yvon.pub")
}

resource "hcloud_ssh_key" "jacques_elitebook" {
  name       = "jacques_elitebook"
  public_key = file("../../../public_keys/jacques.pub")
}

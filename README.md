# Infrastructure management of the InterFoodcoop collective resources

🇫🇷

Interfoodcoop est un collectif monté dans la communauté intercoop entre plusieurs [supermarchés coopératifs](https://forum.supermarches-cooperatifs.fr/) de France dont le but est, à la fois, de fournir des outils et de développer des logiciels libres et open-source pour le bien commun des supermarchés.

Pour plus de détails visitez [le site web de présentation du collectif](https://www.interfoodcoop.net).

🇬🇧
Interfoodcoop is a collective of people initiated in the community inter-cooperatives between multiple [food supermarket cooperatives](https://forum.supermarches-cooperatifs.fr/) in France whose goal is to provide collaborative free and open source tools and also develop open source and free softwares for the common good of supermarkets.

For more details, visit [the presentaiton website](https://www.interfoodcoop.net).

---

This repository holds the whole infrastructure management code. Infrastructure management is done in two main steps:

- Server provisioning (asking some cloud provider to give us some computing resources)
- Server configuration (mainly SSHing into the servers and configure all the tools we need)

## Resources provisioning management

Computing resources are provisioned with Terraform. Terraform is a tool which interacts with many cloud providers to avoid manual actions to create/remove/ resources (such as storage, servers, …).

We use a [wrapper script](https://github.com/paulRbr/terraform-makefile/) to execute every Terraform command for simplification purposes. Please follow the [installation instructions](https://github.com/paulRbr/terraform-makefile/#installation) from the wrapper's readme if you need to interact with our infrasturcture resources.

Secrets are managed manually either via environment variables or via [`pass`](https://www.passwordstore.org/).

Resources are described in HCL language in `*.tf` files. This repo currently has a unique environment:

- `intercoop-cloud` visible in the `providers/hetzner,gandi,scaleway/intercoop-cloud` directory (on Paul's personnal Scaleway & Hetzner account)

### Installation

Please [configure](https://github.com/scaleway/scaleway-sdk-go/blob/master/scw/README.md#scaleway-config) your scaleway credentials locally. (in a `~/.config/scw/config.yaml` file)

Then install Terraform thanks to this command:

```
tf-make provider=hetzner,gandi,scaleway env=intercoop-cloud install
```

(Temporary until the Gandi provider is published in the TF registry) For Gandi resources you will need to download the provider executable (v2.0.0-rc3 is [available here](https://github.com/go-gandi/terraform-provider-gandi/suites/1234962121/artifacts/18628622)) and place it in a local directory ([depends of your platform](https://www.terraform.io/docs/commands/cli-config.html#implied-local-mirror-directories)) inside `github.com/go-gandi/gandi/2.0.0/linux_amd64/`.

### Planning changes

You can do some modification in the `*.tf` files to add/remove/modify resources. Once ready you only need to execute the `plan` command of Terraform:

```
tf-make provider=hetzner,gandi,scaleway env=intercoop-cloud plan
```

Here is an example output planing to create one new resources (a VM instance):

```
------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # hcloud_server.discourse[0] will be created
  + resource "hcloud_server" "discourse" {
      + backup_window = (known after apply)
      + backups       = false
      + datacenter    = (known after apply)
      + id            = (known after apply)
      + image         = "debian-10"
      + ipv4_address  = (known after apply)
      + ipv6_address  = (known after apply)
      + ipv6_network  = (known after apply)
      + keep_disk     = false
      + labels        = {
          + "env" = "intercoop-cloud"
          + "os"  = "debian"
        }
      + location      = "nbg1"
      + name          = "discourse-1"
      + server_type   = "cpx11"
      + ssh_keys      = [
          + "1732338",
        ]
      + status        = (known after apply)
    }

Plan: 1 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------
```

### Applying changes

If happy with your changes you can now apply them with the `apply` command:

```
tf-make provider=hetzner,gandi,scaleway env=intercoop-cloud apply
```

### Current infrastructure layout

#### `intercoop-cloud` environment

2 servers:

- `hcloud_server.nextcloud[0]`: Server hosting PHP applications
- `hcloud_server.discourse[0]`: Server hosting Discourse

1 ssh key:

- `hcloud_ssh_key.paul_polonium`: Paul's SSH key to access servers

15 DNS records:

- `gandi_domain_zone_record.guides-interfoodcoop-net-A[0]`: guides.interfoodcoop.net pointing to the ipv4 of `hcloud_server.nextcloud[0]` server
- `gandi_domain_zone_record.guides-interfoodcoop-net-AAAA[0]`: guides.interfoodcoop.net pointing to the ipv6 of `hcloud_server.nextcloud[0]` server
- `gandi_domain_zone_record.mailjet-guides-interfoodcoop-net-TXT[0]`
- `gandi_domain_zone_record.mailjet-SPF-guides-interfoodcoop-net-TXT[0]`
- `gandi_domain_zone_record.mailjet-DKIM-guides-interfoodcoop-net-TXT[0]`

- `gandi_domain_zone_record.nuage-interfoodcoop-net-A[0]`: nuage.interfoodcoop.net pointing to the ipv4 of `hcloud_server.nextcloud[0]` server
- `gandi_domain_zone_record.nuage-interfoodcoop-net-AAAA[0]`: nuage.interfoodcoop.net pointing to the ipv6 of `hcloud_server.nextcloud[0]` server
- `gandi_domain_zone_record.mailjet-nuage-interfoodcoop-net-TXT[0]`
- `gandi_domain_zone_record.mailjet-SPF-nuage-interfoodcoop-net-TXT[0]`
- `gandi_domain_zone_record.mailjet-DKIM-nuage-interfoodcoop-net-TXT[0]`


## System configuration management

Servers are then configured with Ansible. Ansible is a tool which can configure a set of servers by executing a list of tasks on them.

We use [a wrapper Makefile](https://github.com/paulRbr/ansible-makefile) to launch every ansible commands. You will need to install it locally to launch the commands below. Please follow [the instructions](https://github.com/paulRbr/ansible-makefile#installation) from the wrapper repo readme.

This repo currently manages a single environment (called an `inventory` in Ansible vocabulary):

- `intercoop-cloud` - An environment to receive tools for intercoop work (See details on [the forum](https://forum.supermarches-cooperatifs.fr/t/communaute-de-developpeurs-autour-de-logiciels-libres/1800)) _(Hetzner dynamic inventory)_

### First Installation / Updates

    # Install ansible
    pip install -r requirements.txt
    # Install role dependencies # add args='--force' if you need to update
    ansible-make install roles_path=vendor/

### Dry run a configuration change

    ansible-make dry-run env=<inventory_name>

For example to change configuration of the `intercoop-cloud` instances:

    ansible-make dry-run env=intercoop-cloud

:::warning
We use a dynamic inventory to detect our servers automatically via a remote API access (dynamic inventories). Thus you will need to provide an API token as environment variable to be able to launch the command.
:::

Dynamic inventory example:

    # Hetzner dynamic inventory
    HCLOUD_TOKEN="$(pass terraform/hetzner/intercoop-cloud/token 2>&1)" ansible-make dry-run env=intercoop-cloud

### Deploy changes

Replace the `dry-run` action to `run` in the "Dry run configuration change" paragraph once you are happy to apply your changes.

🚀
